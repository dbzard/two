package com.dbzard.tgs.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.dbzard.tgs.model.ResponseData;
import com.dbzard.tgs.service.GameService;

/**
 * main controller
 * 
 * @author Qiu Dai
 * 
 */
@Controller
public class HomeController {
	
	private static final ObjectMapper mapper = new ObjectMapper();

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private GameService gameService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(HttpServletRequest request, Model model) {
		return "home";
	}
	
	// request a game
	@RequestMapping(value = "/requestGame", method = RequestMethod.GET)
	public void requestGame(@RequestParam(value="playerId",required=false) String playerId, @RequestParam("callback") String callback, HttpServletResponse response) {
		logger.info("player " + playerId + " request Game");	
		ResponseData result = gameService.requestGame(playerId);
		writeResponseData(response, callback, result);
	}
	
	// exchange game data
	@RequestMapping(value = "/exchangeData")
	public void exchangeData(@RequestParam(value="playerId",required=true) String playerId,@RequestParam(value="data",required=false) String data, @RequestParam("callback") String callback, HttpServletResponse response) {
		logger.info(playerId + " exchange game data :" + data);
		ResponseData result = gameService.exchangeData(playerId, data);
		writeResponseData(response, callback, result);
	}
	
	// game reset
	@RequestMapping(value = "/resetGame")
	public void resetGame(@RequestParam(value="playerId",required=true) String playerId, @RequestParam(value="forceReset",required=false) boolean forceReset,
			@RequestParam("callback") String callback, HttpServletResponse response) {
		logger.info("player " + playerId + " reset Game");	
		ResponseData result =  gameService.resetGame(playerId, forceReset);
		writeResponseData(response, callback, result);
	}	

	public GameService getGameService() {
		return gameService;
	}

	public void setGameService(GameService gameService) {
		this.gameService = gameService;
	}
	
	private void writeResponseData(HttpServletResponse response, String callback, ResponseData result) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (out != null) {
			try {
				out.print(callback+"("+mapper.writeValueAsString(result)+")");
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
