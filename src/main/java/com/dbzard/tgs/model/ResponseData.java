package com.dbzard.tgs.model;

/**
 * @author Qiu Dai
 *
 */
public class ResponseData {
	public static String ACTION_REQUEST = "request";
	public static String ACTION_START = "start";
	
	public static String DATA_LOSE = "lose";
	public static String DATA_WIN = "win";
	
	public static String STATUS_OK = "ok";
	public static String STATUS_NG = "ng";
	
	private String status;
	
	private String data;

	private String playerId;
	
	private String action;
	
	public ResponseData(String id) {
		this.playerId = id;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
