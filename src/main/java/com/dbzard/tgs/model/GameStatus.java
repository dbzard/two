package com.dbzard.tgs.model;

/**
 * 
 * @author Qiu Dai
 *
 */
public enum GameStatus {
	STOP, IDLE, READY, RUNNING
}
