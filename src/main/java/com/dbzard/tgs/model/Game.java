package com.dbzard.tgs.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Qiu Dai
 *
 */
public class Game {
	private static final Game game = new Game();
	
	private static final Logger logger = LoggerFactory
			.getLogger(Game.class);
	
	// Game is singleton
	public static Game getInstance() {
		return game;
	}
	
	private Player player1;
	
	private Player player2;
	
	private GameStatus status = GameStatus.STOP;

	public Player getPlayer1() {
		return player1;
	}

	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public void setPlayer2(Player player2) {
		this.player2 = player2;
	}

	public synchronized GameStatus getStatus() {
		return status;
	}

	public synchronized void setStatus(GameStatus status) {
		this.status = status;
	}
	
	public synchronized String addPlayer() {
		if (this.player1 == null) {
			this.player1 = new Player("1");
			logger.debug("create player1");
			return "1";
		} else if (this.player2 == null) {
			this.player2 = new Player("2");
			logger.debug("create player2");
			return "2";
		}
		return null;
	}

}
