package com.dbzard.tgs.model;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.lang.StringUtils;

/**
 * @author Qiu Dai
 *
 */
public class Player {
	
	private String id;

	private Queue<String> dataQueue = new ConcurrentLinkedQueue<String>();
	
	private boolean loseFlag = false;
	
	private boolean readyFlag = false;

	public boolean isReadyFlag() {
		return readyFlag;
	}

	public void setReadyFlag(boolean readyFlag) {
		this.readyFlag = readyFlag;
	}

	public Player(String id) {
		this.id = id;
	}

	public boolean addData(String data) {
		if (StringUtils.isNotEmpty(data)) {
			return this.dataQueue.add(data);
		} else {
			return false;
		}
	}
	
	public void clearData() {
		this.dataQueue.clear();
	}
	
	public String fetchData() {
		return this.dataQueue.poll();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isLoseFlag() {
		return loseFlag;
	}

	public void setLoseFlag(boolean loseFlag) {
		this.loseFlag = loseFlag;
	}
}
