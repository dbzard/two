package com.dbzard.tgs.service;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.dbzard.tgs.model.Game;
import com.dbzard.tgs.model.GameStatus;
import com.dbzard.tgs.model.Player;
import com.dbzard.tgs.model.ResponseData;

/**
 * @author Qiu Dai
 * 
 */
@Service
public class GameServiceImpl implements GameService {

	private static final Logger logger = LoggerFactory
			.getLogger(GameServiceImpl.class);

	@Override
	public ResponseData requestGame(String playerId) {
		Game game = Game.getInstance();
		ResponseData result = null;
		String id = playerId;
		Player p1 = game.getPlayer1();
		Player p2 = game.getPlayer2();
		switch (game.getStatus()) {
		case STOP:
			if (StringUtils.isEmpty(playerId)) {
				id = game.addPlayer();
			}
			
			if (id != null && (id.equals("1") || id.equals("2"))) {
				result = new ResponseData(id);
				result.setStatus(ResponseData.STATUS_OK);
				result.setAction(ResponseData.ACTION_REQUEST);
			}
			
			if (id.equals("1")) {
				game.setStatus(GameStatus.IDLE);
				logger.debug("Game move to READY status");
			}
			
			break;
		
		case IDLE:
			if (StringUtils.isEmpty(playerId)) {
				id = game.addPlayer();
			}
			
			if (id != null && (id.equals("1") || id.equals("2"))) {
				result = new ResponseData(id);
				result.setStatus(ResponseData.STATUS_OK);
				result.setAction(ResponseData.ACTION_REQUEST);
			}
			
			if (id.equals("2")) {
				game.setStatus(GameStatus.READY);
				logger.debug("Game move to READY status");
			}
			break;
			
		case READY:
			
			if (id.equals("1")) {
				result = new ResponseData(id);
				p1.setReadyFlag(true);
			} else if (id.equals("2")) {
				result = new ResponseData(id);
				p2.setReadyFlag(true);
			}
			
			result.setStatus(ResponseData.STATUS_OK);
			if (p1.isReadyFlag() && p2.isReadyFlag()) {
				result.setAction(ResponseData.ACTION_START);
				game.setStatus(GameStatus.RUNNING);
				logger.debug("Game move to RUNNING status");
			} else {
				result.setAction(ResponseData.ACTION_REQUEST);
			}
			break;

		case RUNNING:
			result = new ResponseData(id);
			result.setStatus(ResponseData.STATUS_OK);
			result.setAction(ResponseData.ACTION_START);
			break;

		default:
			break;
		}

		return result;
	}

	@Override
	public ResponseData exchangeData(String playerId, String data) {
		ResponseData resultData = new ResponseData(playerId);
		Game game = Game.getInstance();
		Player player1 = game.getPlayer1();
		Player player2 = game.getPlayer2();
		String playerData = null;
		
		if ("end".equals(data)) {
			if (playerId.equals("1")) {
				if (!player2.isLoseFlag()) {
					player1.setLoseFlag(true);
					player2.setLoseFlag(false);
					player1.clearData();
					player2.clearData();
					player1.addData(ResponseData.DATA_WIN);
					player2.addData(ResponseData.DATA_LOSE);
				}

			} else if (playerId.equals("2")) {
				if (!player1.isLoseFlag()) {
					player1.setLoseFlag(false);
					player2.setLoseFlag(true);
					player1.clearData();
					player2.clearData();
					player1.addData(ResponseData.DATA_LOSE);
					player2.addData(ResponseData.DATA_WIN);
				}
			}
		}
		
		if (playerId.equals("1")) {
			player1.addData(data);
			playerData = player2.fetchData();
		} else if (playerId.equals("2")) {
			player2.addData(data);
			playerData = player1.fetchData();
		}
		resultData.setStatus(ResponseData.STATUS_OK);
		resultData.setData(playerData);

		return resultData;
	}

	@Override
	public ResponseData resetGame(String playerId, boolean forceReset) {
		Game game = Game.getInstance();
		ResponseData data = new ResponseData(playerId);
		if (forceReset && game.getStatus() != GameStatus.IDLE) {
			game.setStatus(GameStatus.STOP);
			game.setPlayer1(null);
			game.setPlayer2(null);
			data.setStatus(ResponseData.STATUS_OK);
		} else {
			data.setStatus(ResponseData.STATUS_NG);
		}
		return data;
	}
}
