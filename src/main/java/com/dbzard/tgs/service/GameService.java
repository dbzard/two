package com.dbzard.tgs.service;

import com.dbzard.tgs.model.ResponseData;

/**
 * @author Qiu Dai
 *
 */
public interface GameService {
	public ResponseData requestGame(String playerId);

	public ResponseData exchangeData(String playerId, String data);
	
	public ResponseData resetGame(String playerId, boolean forceReset);
}
