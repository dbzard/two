(function(win, $){
	
	"use strict";
//	var URL_BASE = "http://localhost:8080/two/";
	var URL_BASE = "http://twogameserver.cloudfoundry.com";
	var URL_REQUEST_GAME = URL_BASE + "/requestGame";
	var URL_EXCHANGE_DATA = URL_BASE + "/exchangeData";
	var URL_RESET_GAME = URL_BASE + "/resetGame";
	var REQUEST_TIME_OUT = 6000;
	
	win.tgs = win.tgs || {};
	var twoGameServer = win.tgs;
	twoGameServer.completeFlag = false; 
	twoGameServer.endGameFlag = false; 
	
	var requestGameCallback = null,
		resetGameCallback = null,
	    dataProvider = null,
		processCallback = null,
		finishCallback = null,
		playerId = null;
	
	twoGameServer.requestGame = function(callback) {
		requestGameCallback = callback || requestGameCallback;
		
		$.jsonp({
			url: URL_REQUEST_GAME + "?callback=?",
			timeout: REQUEST_TIME_OUT,
			data : {"playerId" : playerId},
			success: function (data, textStatus, xOptions) {
				if (data && data.status === "ok") {
					playerId = playerId || data.playerId;
					
					if (data.action === "start") {
						twoGameServer.completeFlag = true;
						if ($.isFunction(requestGameCallback)) {
							requestGameCallback(data);
						}
					}
				}
			},
			error: function (xOptions, textStatus) {
				console.log("request game error:" + textStatus);
				twoGameServer.completeFlag = true;
			},
			complete: function() {
				if (!twoGameServer.completeFlag) {
					twoGameServer.requestGame();
				} else {
					twoGameServer.completeFlag = false;
				}
			}
		});
	};
	
	function dataProveiderWrapper(provider) {
		var temp = "", result = {};
		if ($.isFunction(provider)) {
			temp = provider();
		}
		result.playerId = playerId;
		if (twoGameServer.endGameFlag) {
			temp = "stop";
		}
		result.data = temp;
		return result;
	}
	
	twoGameServer.exchangeData = function(config) {
		config = config || {};
		dataProvider = config.provider || dataProvider;
		processCallback = config.process || processCallback;
		finishCallback = config.finish || finishCallback;
		
		if (!dataProvider) {
			dataProvider = function() {
				return {};
			};
		}
		
		$.jsonp({
			url : URL_EXCHANGE_DATA + "?callback=?",
			timeout: REQUEST_TIME_OUT,
			data : dataProveiderWrapper(dataProvider),
			success: function (data, textStatus, xOptions) {
				if (data && data.status === "ok") {
					if ($.isFunction(processCallback)) {
						processCallback(data);
					}
					
					if(data.data && data.data === "win" || data.data === "lose") {
						twoGameServer.completeFlag = true;
						playerId = null;
						if ($.isFunction(finishCallback)) {
							finishCallback(data.data);
						}
					}
				}
			},
			error: function (xOptions, textStatus) {
				console.log("exchangeData error: " + textStatus);
				twoGameServer.completeFlag = true;
			},
			complete: function() {
				if (!twoGameServer.completeFlag) {
					twoGameServer.exchangeData();
				} else {
					twoGameServer.completeFlag = false;
					twoGameServer.endGameFlag = false;
				}
			}
		});
	};
	
	twoGameServer.resetGame = function(callback, force) {
		resetGameCallback = callback || resetGameCallback;
		var forceReset = !!force;
		
		$.jsonp({
			url : URL_RESET_GAME + "?callback=?",
			timeout: REQUEST_TIME_OUT,
			data : {"playerId" : playerId, "forceReset" : forceReset},			
			success: function (data, textStatus, xOptions) {
				playerId = null;
				if ($.isFunction(resetGameCallback)) {
					resetGameCallback(data);
				}
			},
			error: function (xOptions, textStatus) {
				console.log("resetGame error:" + textStatus);
				if ($.isFunction(resetGameCallback)) {
					resetGameCallback(null);
				}
			}
		});
	};
	
	twoGameServer.endGame = function() {
		twoGameServer.endGameFlag = true;
	};
		
})(this, jQuery);